const initialState = {}

export default (reducer, state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state
