// TODO - As new data becomes available, new data to be added. ('string' or '0' and current booleans are placeholders ONLY)
export function createTransferObject (sellerDetails, buyerDetails) {
  const transferObject = {
    partnerId: 0,
    notificationUrl: 'string',
    transactionTypeId: 'string',
    salePrice: 0,
    upi: 'string',
    isBuyerForeigner: true,
    transactionComments: 'string',
    transactionSellersRemainingOwners: [
      {
        idNo: 0,
        idTypeId: 'string',
        countryId: 'string',
        share: 100,
        IsRepresentative: false
      }
    ],
    transactionBuyerPersons: [],
    transactionParcelRepresentativePersons: [
      {
        personType: 'string',
        idNo: 0,
        idTypeId: 'string',
        countryId: 'string',
        reasonForNoId: 'string',
        surname: 'string',
        forenames: 'string',
        maritalStatus: 'string',
        gender: 'string',
        birthDate: 'string',
        deathDate: 'string',
        fullName: 'string',
        villageCode: 'string',
        boxNo: 'string',
        streetName: 'string',
        addressLine1: 'string',
        placeOfBusiness: 'string',
        nature: 'string',
        representativeFunctionary: 'string',
        tinNumber: 0
      }
    ],
    supportingDocuments: [
      {
        documentDescription: 'My Description',
        document: 'documentData'
      }
    ]
  }
  for (let i = 0; i < buyerDetails.length; i++) {
    transferObject.transactionBuyerPersons.push(
      {
        personType: 'string',
        idNo: buyerDetails[i].data.ids[0].idValue,
        idTypeId: 'NID',
        countryId: 'string',
        reasonForNoId: 'string',
        surname: buyerDetails[i].data.lastNames[0],
        forenames: buyerDetails[i].data.firstNames[0],
        maritalStatus: 'string',
        gender: buyerDetails[i].data.gender,
        birthDate: buyerDetails[i].data.birthDate,
        deathDate: 'string',
        fullName: buyerDetails[i].data.firstNames[0] + ' ' + buyerDetails[i].data.lastNames[0],
        villageCode: 'string',
        boxNo: 'string',
        streetName: 'string',
        addressLine1: 'string',
        placeOfBusiness: 'string',
        nature: 'string',
        representativeFunctionary: 'string',
        tinNumber: 0,
        share: 'string',
        IsRepresentative: true
      }
    )
  }

  return transferObject
}

export function createParcelObject (parcel) {
  const parcelGeoJSON = parcel?.GeoJSON ? JSON.parse(parcel?.GeoJSON) : ''

  const owners = parcel.Owners
    ? parcel.Owners.map(owner => {
        return {
          fullName: owner?.FullName,
          idNo: owner?.IDNo,
          idTypeName: owner?.IDTypeName,
          countryName: 'RWANDA',
          gender: 'Not Available',
          maritalStatus: 'Not Available'
        }
      })
    : []

  const parcelObject =
    {
      upi: parcel?.Upi,
      isUnderRestriction: parcel?.IsUnderRestriction,
      isInTransaction: parcel?.InTransaction,
      hasMortgage: parcel?.IsMortgaged,
      // Below correct??
      size: parcel?.Area,
      landUseNameEnglish: parcel?.LandUseNameEnglish,
      landUseNameKinyarwanda: parcel?.LandUseNameKinyarwanda,
      landUseCode: parcel?.LandUseTypeID,
      rightType: 'Not Available',
      coordinateReferenceSystem: 'Not Available',
      xCoordinate: 'N/A',
      yCoordinate: 'N/A',
      remainingLeaseTerm: 'N/A',
      inProcess: 'Not Available',
      owners: owners,
      representative: {
        surname: 'Not Available',
        foreNames: 'Not Available',
        idNo: 'Not Available',
        idTypeName: 'Not Available',
        countryName: 'Not Available',
        gender: 'Not Available',
        maritalStatus: 'Not Available',
        address: {
          village: {
            villageCode: 'Not Available',
            villageName: 'Not Available'
          },
          cell: {
            cellCode: 'Not Available',
            cellName: 'Not Available'
          },
          sector: {
            sectorCode: 'Not Available',
            sectorName: 'Not Available'
          },
          district: {
            districtCode: 'Not Available',
            districtName: 'Not Available'
          },
          province: {
            provinceCode: 'Not Available',
            provinceName: 'Not Available'
          }
        }
      },
      parcelLocation: {
        cell: {
          cellCode: parcel?.CellCode,
          cellName: parcel?.CellName
        },
        sector: {
          // TODO - parcel.cellCode appears to be longer form of sectorCode, district code etc, when possible check with solomon if these can be derived from cellCode
          sectorCode: 'Not Available',
          sectorName: parcel?.SectorName
        },
        district: {
          districtCode: 'Not Available',
          districtName: parcel?.DistrictName
        },
        province: {
          provinceCode: 'Not Available',
          provinceName: parcel?.ProvinceName
        }
      },
      geoJson: {
        type: parcelGeoJSON?.type,
        coordinates: parcelGeoJSON?.coordinates
      },
      id: 1
      // TODO fields not used (Check) VillageName, IsTransaction. Village name for representative but not owner(?)
    }

  return parcelObject
}

export const createNidaInfoObject = nidaData => {
  if (!nidaData) return null
  // const nameSplit = owner?.FullName?.split(' ')

  const maritalStatuses = status => {
    return ({ S: 0, M: 1, D: 2, W: 3 })[status] || ''
  }

  return {
    data: {
      birthdate: nidaData.DateOfBirth,
      gender: nidaData.Sex,
      maritalStatus: maritalStatuses(nidaData.CivilStatus)
    }
  }
}
