export default stages => {
  const excludedStages = ['Registrar_Approval']

  const completedStages = stages.map(stage => {
    // To be changed into stage.signature when completed
    if (!excludedStages.includes(stage.id)) return !!stage.steps[0].signature

    return true
  })

  return {
    allStagesComplete: !completedStages.includes(false),
    completedStages
  }
}
