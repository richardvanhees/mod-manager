import { INVALID_AUTH_TOKEN } from '../modules/LoginAuth'
import { logout } from '../modules/account/actions/logout.actions'

export function handleResponse (res, defaultResponse) {
  if (res !== undefined && res.status >= 200 && res.status < 300) {
    return res.data
  }
  return defaultResponse
}

export function handleCallResponse (res, defaultResponse) {
  if (res !== undefined && res.status >= 200 && res.status < 300) {
    return {
      success: res.status >= 200 && res.status < 300,
      data: res.data
    }
  }
  return {
    success: false,
    data: defaultResponse
  }
}

export const LOGOUT_TIMEOUT = 5000

export const handleErrorResponse = (dispatch, error, errorCallback = () => {
}) => {
  let shouldLogout = false
  let errorResponse = {}
  if (error.response) {
    if (error.response.status === 404) {
      return
    }
    console.error('Response Error: ', error.response)
    errorResponse = error.response
    if (error.response.status === 401) {
      shouldLogout = true
      errorResponse = { status: 401, message: 'error.invalid_permission_error' }
    } else {
      errorResponse = { message: error.message, status: error.response?.status }
    }
  } else if (error.request) {
    console.error('Request Error: ', error.request)
    // handle service down and invalid permission (Service returns 500 on them )
    errorResponse = { status: 401, message: 'error.invalid_permission_error' }
    shouldLogout = true
  } else {
    errorResponse = { status: 'unknown', message: 'error.unknown_error' }
    console.error('Unknown Error: ', error)
  }
  if (shouldLogout) {
    setTimeout(() => {
      dispatch(logout(INVALID_AUTH_TOKEN))
    }, LOGOUT_TIMEOUT)
  }

  return errorCallback(errorResponse)
}
