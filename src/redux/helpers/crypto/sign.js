import forge from 'node-forge'

const subtle = window.crypto.subtle

const ALGORITHM_TYPE = 'RSASSA-PKCS1-v1_5'
const ALGORITHM_HASH = 'SHA-256'
const KEY_USAGES = ['sign']

function arrayBufferToString (buffer) {
  let binary = ''
  const bytes = new Uint8Array(buffer)

  for (let i = 0; i < bytes.byteLength; i++) {
    binary += String.fromCharCode(bytes[i])
  }
  return binary
}

function stringToArrayBuffer (data) {
  const arrBuff = new ArrayBuffer(data.length)
  const writer = new Uint8Array(arrBuff)
  for (let i = 0, len = data.length; i < len; i++) {
    writer[i] = data.charCodeAt(i)
  }
  return arrBuff
}

function privateKeyToPkcs8 (privateKey) {
  const rsaPrivateKey = forge.pki.privateKeyToAsn1(privateKey)
  const privateKeyInfo = forge.pki.wrapRsaPrivateKey(rsaPrivateKey)
  const privateKeyInfoDer = forge.asn1.toDer(privateKeyInfo).getBytes()
  return stringToArrayBuffer(privateKeyInfoDer)
}

const importCryptoKeyPkcs8 = function * (privateKey, extractable) {
  const privateKeyInfoDerBuff = privateKeyToPkcs8(privateKey)
  return yield subtle.importKey(
    'pkcs8',
    privateKeyInfoDerBuff,
    { name: ALGORITHM_TYPE, hash: { name: ALGORITHM_HASH } },
    extractable,
    KEY_USAGES)
}

export default function * (dataToSign, pfxWithMeta, password) {
  const extractable = true
  const trimmedPfx = pfxWithMeta.split('base64,')
  const pfx = trimmedPfx.length === 1 ? trimmedPfx[0] : trimmedPfx[1]
  const pkcs12Der = forge.util.decode64(pfx)
  const pkcs12Asn1 = forge.asn1.fromDer(pkcs12Der)
  const pkcs12 = forge.pkcs12.pkcs12FromAsn1(pkcs12Asn1, password)

  for (let sci = 0; sci < pkcs12.safeContents.length; ++sci) {
    const safeContents = pkcs12.safeContents[sci]

    for (let sbi = 0; sbi < safeContents.safeBags.length; ++sbi) {
      const safeBag = safeContents.safeBags[sbi]
      const privateKey = safeBag?.key

      if (safeBag.type === forge.pki.oids.pkcs8ShroudedKeyBag) {
        const cryptoKey = yield * importCryptoKeyPkcs8(privateKey, extractable)
        const digestToSign = JSON.stringify(dataToSign)
        const digestToSignBuf = stringToArrayBuffer(digestToSign)

        const signature = yield subtle.sign(
          { name: ALGORITHM_TYPE },
          cryptoKey,
          digestToSignBuf
        )

        return forge.util.encode64(arrayBufferToString(signature))
      }

      // To be defined, no use cases yet
      // if (safeBag.type === forge.pki.oids.keyBag) return null
      // if (safeBag.type === forge.pki.oids.certBag) return null
    }
  }

  return null
}
