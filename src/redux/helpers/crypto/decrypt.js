import CryptoJS from 'crypto-js'

const decrypt = function * (
  base64: string,
  password: string
) {
  const encryptionType = CryptoJS.enc.Utf8

  try {
    const decryptedKey = CryptoJS.AES.decrypt(base64, password)
    const privateKey = decryptedKey.toString(encryptionType)

    return { success: true, key: privateKey }
  } catch (e) {
    console.error(e)

    return { success: false, error: 'Failed to sign using your PKI issued private key' }
  }
}

export default decrypt
