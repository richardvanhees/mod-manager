// Import stuff we need from Redux
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { all } from 'redux-saga/effects'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { createStateSyncMiddleware } from 'redux-state-sync'

// Create our Store
const createStoreFn = ({ options = {}, sagas, reducers }) => {
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [
    thunk,
    sagaMiddleware,
    createStateSyncMiddleware({ whitelist: [] })
  ]

  // Create the logger to log Actions to the console
  if (process.env.NODE_ENV !== 'production') {
    // this will ensure redux logger only logs on the client and not in prod
    const reduxLogger = require('redux-logger')
    const logger = reduxLogger.createLogger({
      collapsed: true,
    })
    middleware.push(logger)
  }

  let composeEnhancers

  // Compose a "name" for the window.
  if (typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      name: options.title || 'LAS',
      trace: true,
      traceLimit: 25
    })
  } else {
    composeEnhancers = compose
  }

  // Use the Middleware and create an "enhancer"
  const enhancer = composeEnhancers(
    applyMiddleware(...middleware)
    // other store enhancers if any
  )

  const rootReducer = combineReducers(reducers)
  const store = createStore(rootReducer, enhancer)

  const rootSaga = function * () {
    yield all([...sagas])
  }

  sagaMiddleware.run(rootSaga)

  return { store }
}

// Export the createStore function AND all other functions from other classes
export default createStoreFn
