import { call } from 'redux-saga/effects'

import storeSetup from './store-setup'
import accountReducer, { accountSagas } from '../modules/account'

const createSagaBundle = bundle => Object.values(bundle).map(saga => call(saga))

// EVENT REGISTRATORS
export default options =>
  storeSetup({
    options,
    sagas: [
      // SAGA LISTENER REGISTRATION
      ...createSagaBundle(accountSagas),
    ],
    reducers: {
      // REDUCER LISTENER REGISTRATION
      account: accountReducer
    }
  })
