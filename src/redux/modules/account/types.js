export default {
  LOGIN: 'account/LOGIN',
  LOGIN_START: 'account/LOGIN_START',
  LOGIN_SUCCESS: 'account/LOGIN_SUCCESS',
  LOGIN_FAIL: 'account/LOGIN_FAIL',

  LOGOUT: 'account/LOGOUT',
  LOGOUT_START: 'account/LOGOUT_START',
  LOGOUT_SUCCESS: 'account/LOGOUT_SUCCESS',
  LOGOUT_FAIL: 'account/LOGOUT_FAIL'
}
