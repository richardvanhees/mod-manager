import types from '../types'

export const loginStart = state => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingIn: true
  }
})

export const loginSuccess = (state, { userData }) => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingIn: false
  },
  data: userData,
  error: null
})

export const loginFail = (state, { error }) => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingIn: false
  },
  error
})

export default {
  [types.LOGIN_START]: loginStart,
  [types.LOGIN_SUCCESS]: loginSuccess,
  [types.LOGIN_FAIL]: loginFail
}
