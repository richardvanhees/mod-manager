import types from '../types'

export const logoutStart = state => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingOut: true
  }
})

export const logoutSuccess = state => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingOut: false
  },
  data: null,
  pki: null,
  error: null
})

export const logoutFail = (state, { error }) => ({
  ...state,
  loaders: {
    ...state.loaders,
    isLoggingOut: false
  },
  error
})

export default {
  [types.LOGOUT_START]: logoutStart,
  [types.LOGOUT_SUCCESS]: logoutSuccess,
  [types.LOGOUT_FAIL]: logoutFail
}
