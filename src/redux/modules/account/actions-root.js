import * as subActions from './actions'
// import types from './types'

const bundledSubActions = Object.values(subActions).reduce((actions, curAction) => ({ ...actions, ...curAction }), {})

export default {
  ...bundledSubActions,
}
