import types from '../types'

export const login = ({ username, password }) => ({
  type: types.LOGIN,
  username,
  password
})

export const loginStart = () => ({
  type: types.LOGIN_START
})

export const loginSuccess = userData => ({
  type: types.LOGIN_SUCCESS,
  userData
})

export const loginFail = error => ({
  type: types.LOGIN_FAIL,
  error
})
