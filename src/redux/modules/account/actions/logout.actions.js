import types from '../types'

export const logout = reason => ({
  type: types.LOGOUT,
  reason
})

export const logoutStart = () => ({
  type: types.LOGOUT_START
})

export const logoutSuccess = () => ({
  type: types.LOGOUT_SUCCESS
})

export const logoutFail = error => ({
  type: types.LOGOUT_FAIL,
  error
})
