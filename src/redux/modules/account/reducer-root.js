import * as subReducers from './reducers'
// import types from './types'

const bundledSubReducers = Object.values(subReducers).reduce((rdc, curRdc) => ({ ...rdc, ...curRdc }), {})

export const initialState = {
  loaders: {
    isLoggingIn: false,
  },
  data: null,
  error: null
}

const reducer = {
  ...bundledSubReducers
}

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state
