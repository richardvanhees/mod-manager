import { put, takeLatest } from 'redux-saga/effects'
// import { post, get } from '../../utils/http-wrapper'
import types from '../types'

import { loginStart, loginSuccess, loginFail } from '../actions/login.actions'

export function * login ({ username, password }) {
  try {
    yield put(loginStart())
    // const response = yield call(callToLogin, { username, password })

    // yield put(loginSuccess(response.user))
    yield put(loginSuccess())
  } catch (error) {
    console.error(error)
    yield put(loginFail(error?.response))
  }
}

export default function * loginSaga () {
  yield takeLatest(types.LOGIN, login)
}
