import { put, takeLatest } from 'redux-saga/effects'
import types from '../types'

import { logoutStart, logoutSuccess, logoutFail } from '../actions/logout.actions'

export function * logout () {
  try {
    yield put(logoutStart())

    yield put(logoutSuccess())
  } catch (error) {
    console.error(error)
    yield put(logoutFail(error?.response))
  }
}

export default function * logoutSaga () {
  yield takeLatest(types.LOGOUT, logout)
}
