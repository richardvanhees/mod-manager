import axios from 'axios'

const requestWrapper = async (method, options) => {
  if (process.env.NODE_ENV === 'test') return
  if (!options) throw new Error('Route has not been set.')

  const jwtToken = localStorage.getItem('jwtToken')

  const response = await axios({
    method,
    url: options.route,
    data: options.data,
    json: options.contentType === 'application/json',
    responseType: options.responseType === 'application/json' ? 'json' : options.responseType,
    withCredentials: !!options.withCredentials,
    auth: options.auth,
    onUploadProgress: progressEvent => {
      const progress = progressEvent.loaded / progressEvent.total * 100
      options.onUploadProgress && options.onUploadProgress(progress)
    },
    headers: {
      ...options.headers,
      Authorization: `Bearer ${jwtToken}`,
      'content-type': options.contentType || 'application/json',
      Accept: options.contentType || 'application/json',
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0'
    }
  })

  const handleResponse = () => {
    if (response.data instanceof ArrayBuffer || response.data instanceof Blob) {
      return {
        fileMeta: response.headers['content-disposition'],
        binary: response.data
      }
    }

    return response.data
  }

  return handleResponse()
}

export const get = data =>
  typeof data === 'string'
    ? requestWrapper('get', { route: data })
    : requestWrapper('get', data)
export const post = data => requestWrapper('post', data)
export const deleteRequest = data => requestWrapper('delete', data)
export const putRequest = data => requestWrapper('put', data)
export const patch = data => requestWrapper('patch', data)
