const { app, session, BrowserWindow } = require('electron')
const path = require('path')
const os = require('os')

const startElectron = (urls) =>
  app.whenReady().then(async () => {
    const reduxDevToolsPath = path.join(
      os.homedir(),
      '/Library/Application Support/Google/Chrome/Default/Extensions/lmhkpmbekcpmknklioeibfkpmmfibljd/2.17.2_0'
    )
    await session.defaultSession.loadExtension(reduxDevToolsPath)

    const win = new BrowserWindow({
      width: 800,
      height: 600,
      titleBarStyle: 'hiddenInset',
      title: 'Transport Fever 2 Mod Manager'
    })

    await win.loadURL(urls.localUrlForBrowser)

  })

module.exports = startElectron
