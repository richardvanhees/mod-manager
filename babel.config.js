module.exports = {
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-proposal-optional-chaining'
  ],
  presets: [
    ['@babel/preset-env', {
      modules: 'commonjs',
      targets: {
        browsers: ['last 2 versions', 'ie >= 10']
      }
    }],
    '@babel/preset-react',
    '@babel/preset-flow'
  ],
  env: {
    development: {
      presets: []
    },
    dev: {
      plugins: [
        '@babel/plugin-transform-runtime'
      ]
    },
    qa: {
      plugins: [
        '@babel/plugin-transform-react-constant-elements',
        '@babel/plugin-transform-react-inline-elements'
      ]
    },
    demo: {
      plugins: [
        '@babel/plugin-transform-react-constant-elements',
        '@babel/plugin-transform-react-inline-elements'
      ]
    },
    production: {
      plugins: [
        '@babel/plugin-transform-react-constant-elements',
        '@babel/plugin-transform-react-inline-elements'
      ]
    }
  }
}
